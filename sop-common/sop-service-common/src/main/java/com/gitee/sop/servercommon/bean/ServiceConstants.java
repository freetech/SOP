package com.gitee.sop.servercommon.bean;

/**
 * @author tanghc
 */
public class ServiceConstants {
    /**
     * zookeeper存放接口路由信息的根目录
     */
    public static final String SOP_SERVICE_ROUTE_PATH = "/com.gitee.sop.route";
}
